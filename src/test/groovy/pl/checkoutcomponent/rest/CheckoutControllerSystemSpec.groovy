package pl.checkoutcomponent.rest

import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.servlet.HandlerExecutionChain
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
import pl.checkoutcomponent.domain.Product
import pl.checkoutcomponent.interceptor.OrderValidator
import pl.checkoutcomponent.repository.OrderRepository
import pl.checkoutcomponent.repository.ProductRepository
import spock.lang.Specification

import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.POST

@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
class CheckoutControllerSystemSpec extends Specification {

    @LocalServerPort private int port

    @Autowired private OrderRepository orderRepository
    @Autowired private ProductRepository productRepository
    @Autowired private ApplicationContext applicationContext

    def appClient

    def setup() {
        appClient = new RESTClient("http://localhost:${port}")
    }

    def 'should create shopping cart and return its id'() {
        when: 'client tries to create new shopping chart'
        def response = createCartApiCall()

        then: 'chart is created and client receives chart id'
        response.status == 201
        orderRepository.exists(response.responseData.str)
    }

    def 'should scan items and return order price with promotion and receipt in the end'() {
        given: 'shopping cart and some products'
        def responseCart = createCartApiCall()
        String orderId = responseCart.responseData.str
        def product1 = new Product(BigDecimal.valueOf(2), 'id1', BigDecimal.ONE, 2)
        def product2 = new Product(BigDecimal.TEN, 'id2', BigDecimal.TEN, 50)
        productRepository.add(product1)
        productRepository.add(product2)

        when: 'client scans item 1'
        def response1 = scanItemApiCall(orderId, product1.id)

        then: 'item 1 is added to product and current price is returned'
        response1.status == 200
        response1.responseData == product1.price

        when: 'client scans item 2'
        def response2 = scanItemApiCall(orderId, product2.id)

        then: 'item 2 is added to product and updated price is returned'
        response2.status == 200
        response2.responseData == product1.price + product2.price - 1

        when: 'client scans item 1 again to gain promotion'
        def response3 = scanItemApiCall(orderId, product1.id)

        then: 'item 1 is added to product and current price is returned'
        response3.status == 200
        response3.responseData == product1.priceAfterThreshold + product2.price - 1

        when: 'client gets receipt'
        def response4 = getReceiptApiCall(orderId)

        then: 'receipt is returned with items and calculated price'
        response4.status == 200
        response4.responseData.price == product1.priceAfterThreshold + product2.price - 1
        response4.responseData.products.size() == 2
        response4.responseData.products.get(product1.id) == 2
        response4.responseData.products.get(product2.id) == 1

    }

    def 'should apply proper interceptors'(HttpMethod method, String path, Class[] interceptors) {
        given:
        MockHttpServletRequest request = new MockHttpServletRequest(method.name(), path)
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class)

        when:
        HandlerExecutionChain chain = mapping.getHandler(request)

        then:
        chain.getInterceptors()
                .collect { interceptor -> interceptor.class }
                .containsAll(interceptors)

        where:
        method | path                            | interceptors
        POST   | '/item/*'                       | [OrderValidator]
        GET    | '/item/*'                       | [OrderValidator]

    }

    private HttpResponseDecorator createCartApiCall() {
        return appClient.post(
                path: '/cart',
                requestContentType: ContentType.JSON
        )
    }

    private HttpResponseDecorator scanItemApiCall(String orderId, String productId) {
        return appClient.post(
                path: '/item/' + orderId,
                body: productId,
                requestContentType: ContentType.JSON
        )
    }

    private HttpResponseDecorator getReceiptApiCall(String orderId) {
        return appClient.get(
                path: '/item/' + orderId,
                requestContentType: ContentType.JSON
        )
    }

}
