package pl.checkoutcomponent.rest

import pl.checkoutcomponent.domain.Order
import pl.checkoutcomponent.domain.Product
import pl.checkoutcomponent.repository.OrderRepository
import pl.checkoutcomponent.repository.ProductRepository
import spock.lang.Specification

class CheckoutControllerSpec extends Specification {

    def controller
    def orderRepository
    def productRepository

    def setup() {
        orderRepository = Mock(OrderRepository.class)
        productRepository = Mock(ProductRepository.class)
        controller = new CheckoutController(
                orderRepository: orderRepository,
                productRepository: productRepository
        )
    }

    def 'should create shopping cart'() {
        given:
        def someId = 'someId'
        orderRepository.create() >> someId

        when:
        def response = controller.createShoppingCart()

        then:
        response.getStatusCode().value() == 201
        response.body == someId
    }

    def 'should scan item'() {
        given:
        def order = new Order()
        def product = new Product(BigDecimal.TEN, 'productId', BigDecimal.TEN, 3)
        productRepository.get(product.id) >> product
        orderRepository.get(order.id) >> order

        when:
        def response = controller.scanItem(order.id, product.id)

        then:
        response.getStatusCode().value() == 200
        response.body == 10
    }

    def 'should get all products and price for order'() {
        given:
        def order = new Order()
        def product1 = new Product(BigDecimal.TEN, 'productId1', BigDecimal.TEN, 3)
        def product2 = new Product(BigDecimal.ONE, 'productId2', BigDecimal.TEN, 3)
        order.addItem(product1)
        order.addItem(product2)
        orderRepository.get(order.id) >> order

        when:
        def response = controller.getReceipt(order.id)

        then:
        response.getStatusCode().value() == 200
        response.body.price == 11
        response.body.products.size() == 2
        response.body.products.get('productId1') == 1
        response.body.products.get('productId2') == 1
    }

}
