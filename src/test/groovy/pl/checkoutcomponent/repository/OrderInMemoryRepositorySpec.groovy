package pl.checkoutcomponent.repository

import spock.lang.Specification

class OrderInMemoryRepositorySpec extends Specification {

    def orderRepository = new OrderInMemoryRepository()

    def 'should create new order with id'() {
        when:
        def orderId = orderRepository.create()

        then:
        orderId.matches('[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}')
        orderRepository.exists(orderId)
    }

}
