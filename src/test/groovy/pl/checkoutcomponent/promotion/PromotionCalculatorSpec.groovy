package pl.checkoutcomponent.promotion

import pl.checkoutcomponent.domain.Order
import pl.checkoutcomponent.domain.Product
import spock.lang.Specification

class PromotionCalculatorSpec extends Specification {

    def promotionCalculator = new PromotionCalculator()
    def product1 = new Product(BigDecimal.TEN, "id1", BigDecimal.valueOf(25), 4)
    def product2 = new Product(BigDecimal.valueOf(50), "id2", BigDecimal.valueOf(60), 2)
    def product3 = new Product(BigDecimal.valueOf(5), "id3", BigDecimal.valueOf(18), 5)

    def 'should calculate price for empty cart'() {
        given:
        def order = new Order()

        when:
        def result = promotionCalculator.recalculatePrice(order)

        then:
        result.intValue() == 0

    }

    def 'should calculate price with no promotions applied'() {
        given:
        def order = new Order()
        order.addItem(product2)
        order.addItem(product3)
        order.addItem(product3)

        when:
        def result = promotionCalculator.recalculatePrice(order)

        then:
        result.intValue() == 50 + 2 * 5
    }

    def 'should calculate price with promotion for quantity applied'() {
        given:
        def order = new Order()
        order.addItem(product2)
        order.addItem(product2)
        order.addItem(product2)
        order.addItem(product3)
        order.addItem(product3)
        order.addItem(product3)
        order.addItem(product3)
        order.addItem(product3)

        when:
        def result = promotionCalculator.recalculatePrice(order)

        then:
        result.intValue() == 60 + 50 + 18
    }

    def 'should calculate price with promotion for products'() {
        given:
        def order = new Order()
        order.addItem(product1)
        order.addItem(product1)
        order.addItem(product1)
        order.addItem(product2)
        order.addItem(product3)
        order.addItem(product3)
        order.addItem(product3)
        order.addItem(product3)

        when:
        def result = promotionCalculator.recalculatePrice(order)

        then:
        result.intValue() == 3 * 10 + 50 + 4 * 5 - 1 - 3 * 2
    }

}
