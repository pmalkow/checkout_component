package pl.checkoutcomponent.interceptor

import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import pl.checkoutcomponent.repository.OrderRepository
import spock.lang.Specification

class OrderValidatorSpec extends Specification {

    def request
    def response
    def orderRepository
    def orderValidator
    def someId = 'someId'

    def setup() {
        orderRepository = Mock(OrderRepository.class)
        orderValidator = new OrderValidator(
                orderRepository: orderRepository
        )
        request = new MockHttpServletRequest()
        request.servletPath = '/item/' + someId
        response = new MockHttpServletResponse()
    }

    def 'should indicate not found when order does not exist'() {
        given:
        orderRepository.exists(someId) >> false

        when:
        def result = orderValidator.preHandle(request, response, null)

        then:
        !result
        response.getStatus() == 404
    }

    def 'should indicate ok when order from params does exist'() {
        given:
        orderRepository.exists(someId) >> true

        when:
        def result = orderValidator.preHandle(request, response, null)

        then:
        result
    }
}
