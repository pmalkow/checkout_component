package pl.checkoutcomponent.domain

import pl.checkoutcomponent.promotion.PromotionCalculator
import spock.lang.Specification

class OrderSpec extends Specification {

    def order
    def product
    def promotionCalculator

    def setup() {
        product = new Product(BigDecimal.ONE, 'id', BigDecimal.ONE, 5)
        promotionCalculator = Mock(PromotionCalculator.class)
        order = new Order(
                promotionCalculator: promotionCalculator
        )
    }

    def 'should create order with initial value and no products'() {
        when:
        def order = new Order()

        then:
        order.chart.isEmpty()
        order.price == BigDecimal.ZERO
    }

    def 'should add item to the order'() {
        given:
        promotionCalculator.recalculatePrice(_ as Order) >> BigDecimal.TEN

        when:
        def price = order.addItem(product)

        then:
        price == BigDecimal.TEN
        order.chart.size() == 1
        order.productCount.size() == 1
        order.productCount.get(product.id) == 1
    }

    def 'should add existing item to the order'() {
        given:
        order.addItem(product)

        when:
        order.addItem(product)

        then:
        order.chart.size() == 1
        order.productCount.size() == 1
        order.productCount.get(product.id) == 2
    }

}
