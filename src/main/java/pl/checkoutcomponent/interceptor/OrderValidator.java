package pl.checkoutcomponent.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.checkoutcomponent.repository.OrderRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class OrderValidator extends HandlerInterceptorAdapter {

	@Autowired private OrderRepository orderRepository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String orderId = request.getServletPath().split("/")[2];
		if (!orderRepository.exists(orderId)) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		return true;
	}
}
