package pl.checkoutcomponent.domain;

import pl.checkoutcomponent.promotion.PromotionCalculator;

import java.math.BigDecimal;
import java.util.*;

public class Order {

	private Set<Product> chart;
	private Map<String, Integer> productCount;
	private String id;
	private BigDecimal price;
	private PromotionCalculator promotionCalculator;

	public Order() {
		chart = new HashSet<>();
		productCount = new HashMap<>();
		id = UUID.randomUUID().toString();
		price = BigDecimal.ZERO;
		promotionCalculator = new PromotionCalculator();
	}

	public String getId() {
		return id;
	}

	public BigDecimal addItem(Product product) {
		chart.add(product);
		productCount.put(product.getId(), productCount.getOrDefault(product.getId(), 0) + 1);
		price = promotionCalculator.recalculatePrice(this);
		return price;
	}

	public Set<Product> getChart() {
		return chart;
	}

	public Map<String, Integer> getProductCount() {
		return productCount;
	}

	public BigDecimal getPrice() {
		return price;
	}
}
