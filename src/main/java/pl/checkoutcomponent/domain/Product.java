package pl.checkoutcomponent.domain;

import java.math.BigDecimal;

public class Product {

	private BigDecimal price;
	private String id;
	private int threshold;
	private BigDecimal priceAfterThreshold;

	public Product(BigDecimal price, String id, BigDecimal priceAfterThreshold, int threshold) {
		this.price = price;
		this.id = id;
		this.priceAfterThreshold = priceAfterThreshold;
		this.threshold = threshold;
	}

	public int getThreshold() {
		return threshold;
	}

	public BigDecimal getPriceAfterThreshold() {
		return priceAfterThreshold;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public String getId() {
		return id;
	}
}
