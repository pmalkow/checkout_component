package pl.checkoutcomponent.repository;

import org.springframework.stereotype.Repository;
import pl.checkoutcomponent.domain.Order;

import java.util.HashMap;
import java.util.Map;

@Repository
public class OrderInMemoryRepository implements OrderRepository {

	private Map<String, Order> orders = new HashMap<>();

	@Override
	public String create() {
		Order order = new Order();
		orders.put(order.getId(), order);
		return order.getId();
	}

	@Override
	public Order get(String id) {
		return orders.get(id);
	}

	@Override
	public boolean exists(String id) {
		return orders.containsKey(id);
	}
}
