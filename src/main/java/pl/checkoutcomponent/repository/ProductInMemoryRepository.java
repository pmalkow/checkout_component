package pl.checkoutcomponent.repository;

import org.springframework.stereotype.Repository;
import pl.checkoutcomponent.domain.Product;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ProductInMemoryRepository implements ProductRepository {

	private Map<String, Product> products = new HashMap<>();

	@Override
	public Product get(String id) {
		return products.get(id);
	}

	@Override
	public boolean exists(String id) {
		return products.containsKey(id);
	}

	@Override
	public void add(Product product) {
		products.put(product.getId(), product);
	}
}
