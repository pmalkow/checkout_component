package pl.checkoutcomponent.repository;

import pl.checkoutcomponent.domain.Order;

public interface OrderRepository {

	String create();

	Order get(String id);

	boolean exists(String id);
}
