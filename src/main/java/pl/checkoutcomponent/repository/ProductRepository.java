package pl.checkoutcomponent.repository;

import pl.checkoutcomponent.domain.Product;

public interface ProductRepository {

	Product get(String id);

	boolean exists(String id);

	void add(Product product);
}
