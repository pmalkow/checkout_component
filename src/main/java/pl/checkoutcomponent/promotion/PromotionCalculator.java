package pl.checkoutcomponent.promotion;

import pl.checkoutcomponent.domain.Order;
import pl.checkoutcomponent.domain.Product;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PromotionCalculator {

	private static Map<List<String>, BigDecimal> discounts;

	static {
		discounts = new HashMap<>();
		discounts.put(Arrays.asList("id1", "id2"), BigDecimal.valueOf(1));
		discounts.put(Arrays.asList("id1", "id3"), BigDecimal.valueOf(2));
		discounts.put(Arrays.asList("id1", "id4"), BigDecimal.valueOf(5));
	}

	public BigDecimal recalculatePrice(Order order) {
		BigDecimal price = calculatePromotionBasedOnProductsQuantity(order);
		price = calculatePromotionBasedOnProducts(order, price);

		return price;
	}

	private BigDecimal calculatePromotionBasedOnProducts(Order order, BigDecimal price) {
		for (Map.Entry<List<String>, BigDecimal> entry : discounts.entrySet()) {
			List<String> ids = entry.getKey();
			if (promotionApplies(ids, order.getProductCount())) {
				int howManyTimesToDeduct = minQuantityFromProducts(order.getProductCount(), ids);
				BigDecimal subtrahend = BigDecimal.valueOf(howManyTimesToDeduct).multiply(entry.getValue());
				price = price.subtract(subtrahend);
			}
		}
		return price;
	}

	private int minQuantityFromProducts(Map<String, Integer> productCount, List<String> ids) {
		int min = Integer.MAX_VALUE;
		for (String id : ids) {
			min = Math.min(min, productCount.get(id));
		}
		return min;
	}

	private boolean promotionApplies(List<String> ids, Map<String, Integer> productCount) {
		for (String id : ids) {
			if (!productCount.containsKey(id)) {
				return false;
			}
		}
		return true;
	}

	private BigDecimal calculatePromotionBasedOnProductsQuantity(Order order) {
		BigDecimal price = BigDecimal.ZERO;
		for (Product product : order.getChart()) {
			int quantity = order.getProductCount().get(product.getId());
			price = calculateSpecialPrice(price, product, quantity);
			price = calculateNormalPrice(price, product, quantity);
		}
		return price;
	}

	private BigDecimal calculateNormalPrice(BigDecimal price, Product product, int quantity) {
		BigDecimal normalPriceQuantity = new BigDecimal(quantity % product.getThreshold());
		price = price.add(normalPriceQuantity.multiply(product.getPrice()));
		return price;
	}

	private BigDecimal calculateSpecialPrice(BigDecimal price, Product product, int quantity) {
		BigDecimal specialPriceQuantity = new BigDecimal(quantity / product.getThreshold());
		price = price.add(specialPriceQuantity.multiply(product.getPriceAfterThreshold()));
		return price;
	}

}
