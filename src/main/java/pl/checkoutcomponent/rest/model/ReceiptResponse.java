package pl.checkoutcomponent.rest.model;

import java.math.BigDecimal;
import java.util.Map;

public class ReceiptResponse {

	private Map<String, Integer> products;
	private BigDecimal price;

	public ReceiptResponse(Map<String, Integer> products, BigDecimal price) {
		this.products = products;
		this.price = price;
	}

	public Map<String, Integer> getProducts() {
		return products;
	}

	public BigDecimal getPrice() {
		return price;
	}
}
