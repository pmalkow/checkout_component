package pl.checkoutcomponent.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.checkoutcomponent.domain.Order;
import pl.checkoutcomponent.domain.Product;
import pl.checkoutcomponent.repository.OrderRepository;
import pl.checkoutcomponent.repository.ProductRepository;
import pl.checkoutcomponent.rest.model.ReceiptResponse;

import java.math.BigDecimal;

@RestController
public class CheckoutController {

	@Autowired private OrderRepository orderRepository;
	@Autowired private ProductRepository productRepository;

	@RequestMapping(value = "/cart", method = RequestMethod.POST)
	ResponseEntity<String> createShoppingCart() {
		String orderId = orderRepository.create();
		return new ResponseEntity<>(orderId, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/item/{orderId}", method = RequestMethod.POST)
	ResponseEntity<BigDecimal> scanItem(@PathVariable String orderId, @RequestBody String productId) {
		Order order = orderRepository.get(orderId);
		Product product = productRepository.get(productId);
		BigDecimal currentPrice = order.addItem(product);
		return new ResponseEntity<>(currentPrice, HttpStatus.OK);
	}

	@RequestMapping(value = "/item/{orderId}", method = RequestMethod.GET)
	ResponseEntity<ReceiptResponse> getReceipt(@PathVariable String orderId) {
		Order order = orderRepository.get(orderId);
		ReceiptResponse response = new ReceiptResponse(order.getProductCount(), order.getPrice());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
