# README #
Checkout Component 3.0

Checkout component has 3 REST APIs exposed:

* /cart - POST method - create empty shopping cart and return its unique id which is required for each item scan and receipt print;
* /item/{order_id} - POST method - scans single item and returns combined price for all scanned items so far in single shopping cart; requires orderId as parameter and productId in the body, retrieved by /cart method; all promotions are calculated on the fly, during each item scanned; 404 is returned if given order_id is not found
* /item/{order_id} - GET method - returns receipt with final price and list of all scanned products along with its quantity; 404 is returned if given order_id is not found

Assumptions:

* storage is done in-memory, in case of service restart, all orders are gone
* there are two kind of promotions- when it comes to product quantity and product combination; product quantity promotion can be created within the products; product combination needs to be added separately- for sake of this exercises some of them are hardcoded within 'PromotionCalculator' class, there is no mechanism to add new ones in different way (room for improvement)
* product_id is treaded as always valid- as client should receive list of products from the same storage, validating it seems to be overkill; can be changed in case of other requirements

Tests:

* all unit and system tests are written with Spock framework
* system tests can be found in CheckoutControllerSystemSpec with documentation; each step has proper narrative section and can be used to explore APIs

Deployment:

* as it is application on SpringBoot, to start the service main method from ServerApplication needs to be run
* 'bootRun' gradle task can be executed as well to get application up and running